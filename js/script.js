(function(window, $){         
    function scrollTo(target) {
        var wheight = $(window).height() / 2;
        var alto = $("#header").outerHeight();
        
        var ooo = $(target).offset().top - alto;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    function ajustarVisibilidadMenu(){        
        if (!$("#top-menu-resp").is(":visible")){
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + ($(window).height());
            var elemTop = $("#header").offset().top;
            var elemBottom = elemTop + $("#header").height();
            if ($("#header").height() <  docViewBottom && docViewTop > 0){
                $("#header").addClass("header_fijo");                
                $("body").css("padding-top", $("#header").height());
            }
            else{
                $("#header").removeClass("header_fijo");
                $("body").css("padding-top", 0);
            }
        }
        else{
            $("#header").removeClass("header_fijo");
        }
    }
    
    $(document).ready(function() {
        $(window).resize(function() {
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul:first-child").hide();
            }
            else{                    
                $("#header-top ul:first-child").show();
            }
        });
        
        $("a[rel='grupo']").click(function(e) {
            e.preventDefault();
            var $data = $(this);
            var $dataGrupo = $(this).attr("data-grupo");
            var $animar = $(this).attr("data-animar") ? "slow" : "";
        
            $.each($("a[data-grupo='" + $dataGrupo + "']"), function(i, o) {
                if ($(o) !== $data && $(o).hasClass("activo")){
                    $(o).removeClass("activo");
                    var $href = $(o).attr("href");
                    $($href).hide($animar);
                    
                    var $href2 = $(o).attr("data-href2");
                    if ($href2){
                        $($href2).fadeOut();
                    }
                }
            });
            
            var $href = $(this).attr("href");
            $($href).show($animar);
            
            var $href2 = $(this).attr("data-href2");
            if ($href2){
                $($href2).fadeIn();
            }
            
            $(this).addClass("activo");
        });
        
        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul:first-child").toggle("slow");
        });
        
        $("a[rel='scroll']").click(function(e){
            e.preventDefault();            
            scrollTo($($(this).attr("href")));
        });
        
        $('#gal-foto a').magnificPopup({
            type: 'image',            
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below
            gallery:{
                enabled:true,                
                tPrev: 'Anterior',
                tNext: 'Sigiente',
                tCounter: '<span class="mfp-counter">%curr% de %total%</span>' // markup of counter            
            }
        });
        
        $("a[rel='video']").magnificPopup({
            type: 'iframe'
        });
        
        if ($("#banner_home").length){
            var $sliderHome = $("#banner_home").lightSlider({
                adaptiveHeight:true,
                item:1,
                slideMargin:0,
                pager: true,
                enableDrag: true,
                controls: true,
                loop: true,
                auto: true,
                pause: 8000
            });
                    
            $sliderHome.refresh();
            //$sliderHome.play();
        }
        
        if ($("#slider_plastico").length){
            var $sliderPlastico = $("#slider_plastico").lightSlider({
                adaptiveHeight:true,
                item:1,
                slideMargin:0,
                pager: false,
                enableDrag: true,
                controls: false,
                loop: true,
                mode: 'fade',
                auto: true,
                pause: 5000
            });

            $sliderPlastico.refresh();
            //$sliderIpad.play();
        }
        
        if ($("#slider_productos").length){
            var $sliderProductos = $("#slider_productos").lightSlider({
                adaptiveHeight:true,
                item:1,
                slideMargin:0,
                pager: false,
                enableDrag: true,
                controls: true,
                loop: true,
                mode: 'fade',
                auto: true,
                pause: 5000
            });

            $sliderProductos.refresh();
            //$sliderIpad.play();
        }
        
        $("#btnEnviarSol").click(function(e) {
            e.preventDefault();
            
            if ($("#nombre_contacto").val() === ""){
                alert('Debe ingresar su nombre');
                return;
            }
            
            if ($("#email_contacto").val() === ""){
                alert('Debe ingresar su dirección de correo');
                return;
            }
            
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (!re.test($("#email_contacto").val())){
                alert('Debe ingresar una dirección de correo válida');
                return;
            }
            
            if ($("#mensaje_contacto").val() === ""){
                alert('Debe ingresar un mensaje');
                return;
            }
            
            $("#loader").addClass("loading");
            
            $.ajax({                
                url: $("#formContacto").attr("action"),
                method: 'post',
                data: $("#formContacto").serialize(),
                success: function(res){
                    $("#loader").removeClass("loading");
                    $("#nombre_contacto").val('');
                    $("#email_contacto").val('');
                    $("#mensaje_contacto").val('');
                    alert(res);
                }
                
            });
        });
        
        $("#btnSuscripcion").click(function(e) {
            e.preventDefault();
            
            if ($("#email_news").val() === ""){
                alert('Debe ingresar su dirección de correo');
                return;
            }
            
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (!re.test($("#email_news").val())){
                alert('Debe ingresar una dirección de correo válida');
                return;
            }
            
            $("#loader").addClass("loading");
            
            $.ajax({                
                url: $(this).attr("href"),
                method: 'post',
                data: { email : $("#email_news").val() },
                success: function(res){
                    $("#loader").removeClass("loading");
                    $("#email_news").val('');
                    alert(res);
                }
                
            });
        });        
                
        Foundation.global.namespace = '';
        $(document).foundation();
    });
    
    $(window).load(function() {
        ajustarVisibilidadMenu();        
        
        $(window).scroll(function () {
            ajustarVisibilidadMenu();
        });
    });
})(window, jQuery);