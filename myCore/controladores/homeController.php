<?php

class homeController extends myController{
    public function index(){
        return myView::render("home.index");
    }
    
    public function educacion(){
        return myView::render("home.linea_educativa");
    }
    
    public function salud(){
        return myView::render("home.linea_salud");
    }
    
    public function variedad(){
        return myView::render("home.linea_variedad");
    }

    public function cocina(){
        return myView::render("home.linea_cocina");
    }
    
    public function literatura(){
        return myView::render("home.linea_literatura");
    }
    
    public function saludar($name, $apellido){
        return "Hola ".$name." ".$apellido." :)";
    }
}