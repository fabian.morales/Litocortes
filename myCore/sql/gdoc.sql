create table arc_my_gdoc_categoria(
	id integer not null auto_increment primary key,
	nombre varchar(60),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE InnoDB;

create table arc_my_gdoc_documento(
	id integer not null auto_increment primary key,
	nombre varchar(60),	
    extension varchar(20),
    descripcion text,
    visibilidad enum('P', 'U'), /* (P)rivado, P(u)blico */
    fecha date,
    publicado char(1),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE InnoDB;

create table arc_my_gdoc_catdoc(
	id_categoria integer,
	id_documento integer,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
	foreign key (id_documento) references arc_my_gdoc_documento (id),
	foreign key (id_categoria) references arc_my_gdoc_categoria (id)
) ENGINE InnoDB;